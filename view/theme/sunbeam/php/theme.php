<?php

/**
 *   * Name: Sunbeam
 *   * Description: A fresh theme based on redbasic
 *   * Version: 1.0
 *   * MinVersion: 2.3
 *   * MaxVersion: 3.0 
 *   * Author: Sean Tilley
 *   * Compat: Red [*]
 *
 */

function sunbeam_init(&$App) {

    App::$theme_info['extends'] = 'redbasic';


}
