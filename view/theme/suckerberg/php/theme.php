<?php

/**
 *   * Name: Suckerberg
 *   * Description: A Facebook-like theme based on redbasic
 *   * Version: 2.0
 *   * MinVersion: 2.3
 *   * MaxVersion: 3.0 
 *   * Author: Sean Tilley
 *   * Compat: Hubzilla [*]
 *
 */

function suckerberg_init(&$App) {

    App::$theme_info['extends'] = 'redbasic';


}
