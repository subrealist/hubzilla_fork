<?php

/**
 *   * Name: Nova
 *   * Description: A new style for based on Peoria.in
 *   * Version: 1.0
 *   * MinVersion: 2.3
 *   * MaxVersion: 3.0
 *   * Author: Sean Tilley
 *   * Compat: Red [*]
 *
 */

function nova_init(&$App) {

    App::$theme_info['extends'] = 'redbasic';


}
