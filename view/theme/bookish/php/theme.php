<?php

/**
 *   * Name: Bookish
 *   * Description: A Blogging theme for Hubzilla!
 *   * Version: 1.0
 *   * MinVersion: 2.3
 *   * MaxVersion: 3.0
 *   * Author: Sean Tilley
 *   * Compat: Red [*]
 *
 */

function bookish_init(&$App) {

    App::$theme_info['extends'] = 'redbasic';


}
