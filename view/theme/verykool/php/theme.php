<?php

/**
 *   * Name: VeryKool
 *   * Description: An interface clone of the VK social network. For, uh, you know, Russians.
 *   * Version: 1.0
 *   * MinVersion: 2.3
 *   * MaxVersion: 3.0 
 *   * Author: Sean Tilley
 *   * Compat: Red [*]
 *
 */

function verykool_init(&$App) {

    App::$theme_info['extends'] = 'redbasic';


}
