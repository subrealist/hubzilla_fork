<?php

/**
 *   * Name: Occupant
 *   * Description: An Occupy theme.
 *   * Version: 1.0
 *   * MinVersion: 2.3
 *   * MaxVersion: 3.0
 *   * Author: Sean Tilley
 *   * Compat: Red [*]
 *
 */

function occupant_init(&$App) {

    App::$theme_info['extends'] = 'redbasic';


}
