<?php

/**
 *   * Name: Clarity
 *   * Description: Hubzilla standard theme
 *   * Version: 2.1
 *   * MinVersion: 2.3.1
 *   * MaxVersion: 3.0
 *   * Author: Fabrixxm
 *   * Maintainer: Mike Macgirvin
 *   * Maintainer: Mario Vavti
 */

function clarity_init(&$a) {
  App::$theme_info['extends'] = 'redbasic';

}
